"use strict";
const common_vendor = require("../../common/vendor.js");
const store_user = require("../../store/user.js");
if (!Array) {
  const _easycom_up_tabbar_item2 = common_vendor.resolveComponent("up-tabbar-item");
  const _easycom_up_tabbar2 = common_vendor.resolveComponent("up-tabbar");
  (_easycom_up_tabbar_item2 + _easycom_up_tabbar2)();
}
const _easycom_up_tabbar_item = () => "../../node-modules/uview-plus/components/u-tabbar-item/u-tabbar-item.js";
const _easycom_up_tabbar = () => "../../node-modules/uview-plus/components/u-tabbar/u-tabbar.js";
if (!Math) {
  (_easycom_up_tabbar_item + _easycom_up_tabbar)();
}
const _sfc_main = {
  __name: "tabbar",
  setup(__props) {
    let useStore = store_user.useUserStore();
    const tabbarItems = [
      {
        "pagePath": "../../pages/index/index",
        "iconPath": "map",
        "selectedIconPath": "map-fill",
        "text": "寻梦"
      },
      {
        "pagePath": "../../pages/test_page/index",
        "iconPath": "gift",
        "selectedIconPath": "gift-fill",
        "text": "创作"
      },
      {
        "pagePath": "../../pages/user/index",
        "iconPath": "account",
        "selectedIconPath": "account-fill",
        "text": "我的"
      }
    ];
    const handleTabbarItemClick = (item, index) => {
      if (useStore.activeTab !== index) {
        useStore.setActive(index);
        const path = item.pagePath;
        common_vendor.index.switchTab({
          url: path
        });
      }
    };
    const getTabbarIcon = (item, index) => {
      return useStore.activeTab === index ? item.selectedIconPath : item.iconPath;
    };
    return (_ctx, _cache) => {
      return {
        a: common_vendor.f(tabbarItems, (item, index, i0) => {
          return {
            a: index,
            b: common_vendor.o(($event) => handleTabbarItemClick(item, index), index),
            c: "352016b3-1-" + i0 + ",352016b3-0",
            d: common_vendor.p({
              icon: getTabbarIcon(item, index),
              text: item.text
            })
          };
        }),
        b: common_vendor.p({
          value: common_vendor.unref(useStore).activeTab,
          fixed: true,
          placeholder: true,
          safeAreaInsetBottom: true
        })
      };
    };
  }
};
const Component = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "/Users/yicaohuang/Downloads/dream_star/src/components/tabbar/tabbar.vue"]]);
wx.createComponent(Component);
