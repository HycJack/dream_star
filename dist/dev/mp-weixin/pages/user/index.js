"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      tabBar: "",
      currentIndex: 1,
      pic: "https://uviewui.com/common/logo.png",
      show: true
    };
  },
  onLoad() {
    this.tabBar = [
      {
        "pagePath": "pages/index/index",
        "iconPath": "home",
        "selectedIconPath": "home-fill",
        "text": "寻梦"
      },
      {
        "pagePath": "pages/user/index",
        "iconPath": "home",
        "selectedIconPath": "home-fill",
        "text": "创作"
      },
      {
        "pagePath": "pages/test_page/index",
        "iconPath": "home",
        "selectedIconPath": "home-fill",
        "text": "我的"
      }
    ];
  },
  methods: {
    back() {
      common_vendor.index.navigateBack({
        delta: 1
      });
    }
  }
};
if (!Array) {
  const _easycom_uni_nav_bar2 = common_vendor.resolveComponent("uni-nav-bar");
  const _easycom_up_cell2 = common_vendor.resolveComponent("up-cell");
  const _easycom_up_cell_group2 = common_vendor.resolveComponent("up-cell-group");
  const _easycom_tabbar2 = common_vendor.resolveComponent("tabbar");
  (_easycom_uni_nav_bar2 + _easycom_up_cell2 + _easycom_up_cell_group2 + _easycom_tabbar2)();
}
const _easycom_uni_nav_bar = () => "../../node-modules/@dcloudio/uni-ui/lib/uni-nav-bar/uni-nav-bar.js";
const _easycom_up_cell = () => "../../node-modules/uview-plus/components/u-cell/u-cell.js";
const _easycom_up_cell_group = () => "../../node-modules/uview-plus/components/u-cell-group/u-cell-group.js";
const _easycom_tabbar = () => "../../components/tabbar/tabbar.js";
if (!Math) {
  (_easycom_uni_nav_bar + _easycom_up_cell + _easycom_up_cell_group + _easycom_tabbar)();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.p({
      dark: true,
      fixed: true,
      shadow: true,
      ["background-color"]: "#007AFF",
      ["status-bar"]: true,
      title: "个人中心"
    }),
    b: common_vendor.p({
      icon: "rmb-circle",
      title: "支付"
    }),
    c: common_vendor.p({
      icon: "star",
      title: "收藏"
    }),
    d: common_vendor.p({
      icon: "photo",
      title: "相册"
    }),
    e: common_vendor.p({
      icon: "coupon",
      title: "卡券"
    }),
    f: common_vendor.p({
      icon: "heart",
      title: "关注"
    }),
    g: common_vendor.p({
      icon: "setting",
      title: "设置"
    })
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "/Users/yicaohuang/Downloads/dream_star/src/pages/user/index.vue"]]);
wx.createPage(MiniProgramPage);
