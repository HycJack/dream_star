"use strict";
const common_vendor = require("../../common/vendor.js");
if (!Array) {
  const _easycom_uni_nav_bar2 = common_vendor.resolveComponent("uni-nav-bar");
  const _easycom_up_modal2 = common_vendor.resolveComponent("up-modal");
  const _easycom_up_icon2 = common_vendor.resolveComponent("up-icon");
  const _easycom_up_image2 = common_vendor.resolveComponent("up-image");
  const _easycom_up_button2 = common_vendor.resolveComponent("up-button");
  const _easycom_uni_badge2 = common_vendor.resolveComponent("uni-badge");
  const _easycom_tabbar2 = common_vendor.resolveComponent("tabbar");
  (_easycom_uni_nav_bar2 + _easycom_up_modal2 + _easycom_up_icon2 + _easycom_up_image2 + _easycom_up_button2 + _easycom_uni_badge2 + _easycom_tabbar2)();
}
const _easycom_uni_nav_bar = () => "../../node-modules/@dcloudio/uni-ui/lib/uni-nav-bar/uni-nav-bar.js";
const _easycom_up_modal = () => "../../node-modules/uview-plus/components/u-modal/u-modal.js";
const _easycom_up_icon = () => "../../node-modules/uview-plus/components/u-icon/u-icon.js";
const _easycom_up_image = () => "../../node-modules/uview-plus/components/u-image/u-image.js";
const _easycom_up_button = () => "../../node-modules/uview-plus/components/u-button/u-button.js";
const _easycom_uni_badge = () => "../../node-modules/@dcloudio/uni-ui/lib/uni-badge/uni-badge.js";
const _easycom_tabbar = () => "../../components/tabbar/tabbar.js";
if (!Math) {
  (_easycom_uni_nav_bar + _easycom_up_modal + _easycom_up_icon + _easycom_up_image + _easycom_up_button + _easycom_uni_badge + _easycom_tabbar)();
}
const _sfc_main = {
  __name: "index",
  setup(__props) {
    common_vendor.ref(true);
    common_vendor.ref([
      {
        "pagePath": "pages/index/index",
        "iconPath": "home",
        "selectedIconPath": "home-fill",
        "text": "寻梦"
      },
      {
        "pagePath": "pages/user/index",
        "iconPath": "home",
        "selectedIconPath": "home-fill",
        "text": "创作"
      },
      {
        "pagePath": "pages/test_page/index",
        "iconPath": "home",
        "selectedIconPath": "home-fill",
        "text": "我的"
      }
    ]);
    common_vendor.ref("杭州");
    common_vendor.ref("#001f3f");
    const src = common_vendor.ref("https://cdn.uviewui.com/uview/album/1.jpg");
    const click = () => {
    };
    const show = common_vendor.ref(false);
    function close() {
      show.value = false;
    }
    const title = common_vendor.ref("标题");
    const content = common_vendor.ref(`空山新雨后<br>  
                      天气晚来秋`);
    return (_ctx, _cache) => {
      return {
        a: common_vendor.p({
          dark: true,
          fixed: true,
          shadow: true,
          ["background-color"]: "#007AFF",
          ["status-bar"]: true,
          title: "创作中心"
        }),
        b: content.value,
        c: common_vendor.o(close),
        d: common_vendor.p({
          show: show.value,
          title: title.value,
          closeOnClickOverlay: true
        }),
        e: common_vendor.p({
          name: "photo",
          color: "#2979ff",
          size: "28"
        }),
        f: common_vendor.o(click),
        g: common_vendor.p({
          ["show-loading"]: true,
          src: src.value,
          width: "80px",
          height: "80px"
        }),
        h: common_vendor.p({
          type: "primary",
          plain: true,
          text: "镂空"
        }),
        i: common_vendor.o(($event) => show.value = true),
        j: common_vendor.p({
          type: "success"
        }),
        k: common_vendor.p({
          text: "1"
        }),
        l: common_vendor.o(_ctx.bindClick),
        m: common_vendor.p({
          text: "2",
          type: "success"
        }),
        n: common_vendor.p({
          text: "3",
          type: "primary",
          inverted: true
        })
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "/Users/yicaohuang/Downloads/dream_star/src/pages/test_page/index.vue"]]);
wx.createPage(MiniProgramPage);
