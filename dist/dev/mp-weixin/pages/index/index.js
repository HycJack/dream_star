"use strict";
const common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  data() {
    return {
      cardPositionList: [{ "title": "SS级卡片等待解锁", "location": { "lat": "39.909", "lng": "116.39742" } }],
      tabBar: "",
      currentIndex: 0,
      title: "map",
      latitude: 39.9,
      // 默认纬度
      longitude: 116.312,
      // 默认经度(北京天安门)
      covers: [{
        id: 110,
        latitude: 39.9,
        longitude: 116.397,
        callout: {
          //自定义标记点上方的气泡窗口 点击有效  
          content: "SSS级卡片等待解锁",
          //文本
          color: "#ffffff",
          //文字颜色
          fontSize: 14,
          //文本大小
          borderRadius: 15,
          //边框圆角
          borderWidth: "10",
          bgColor: "#F2B54A",
          //背景颜色
          display: "ALWAYS"
          //常显
        },
        iconPath: "	https://hellouniapp.dcloud.net.cn/static/location.png",
        width: "30",
        height: "30",
        alpha: 0.5
      }, {
        id: 2,
        latitude: 39.8,
        longitude: 116.4,
        callout: {
          //自定义标记点上方的气泡窗口 点击有效  
          content: "A级卡片等待解锁",
          //文本
          color: "#ffffff",
          //文字颜色
          fontSize: 14,
          //文本大小
          borderRadius: 15,
          //边框圆角
          borderWidth: "10",
          bgColor: "#A5E718",
          //背景颜色
          display: "ALWAYS"
          //常显
        },
        iconPath: "	https://hellouniapp.dcloud.net.cn/static/location.png",
        width: "30",
        height: "30",
        alpha: 0.5
      }, {
        id: 1,
        latitude: 39.9,
        longitude: 116.2,
        callout: {
          //自定义标记点上方的气泡窗口 点击有效  
          content: "SS级卡片等待解锁",
          //文本
          color: "#ffffff",
          //文字颜色
          fontSize: 14,
          //文本大小
          borderRadius: 15,
          //边框圆角
          borderWidth: "10",
          bgColor: "#e51860",
          //背景颜色
          display: "ALWAYS"
          //常显
        },
        iconPath: "	https://hellouniapp.dcloud.net.cn/static/location.png",
        width: "30",
        height: "30",
        alpha: 0.5
      }],
      controls: [{
        // 控件
        id: 99,
        position: {
          // 控件位置
          left: 160,
          top: 120
        },
        iconPath: "https://hellouniapp.dcloud.net.cn/static/location.png"
        // 控件图标
      }],
      address_info: "",
      address_info_recomd: "",
      address: ""
    };
  },
  onLoad() {
  },
  methods: {
    clickLeft() {
      var _this = this;
      _this.getLocationInfo();
    },
    click1(e) {
      common_vendor.route({ url: "pages/user/index", type: "tab" });
    },
    isGetLocation(a = "scope.userLocation") {
      var _this = this;
      common_vendor.index.getSetting({
        success(res) {
          console.log(res);
          if (!res.authSetting[a]) {
            _this.getAuthorizeInfo();
          } else {
            _this.getLocation();
          }
        }
      });
    },
    getAuthorizeInfo(a = "scope.userLocation") {
      var _this = this;
      common_vendor.index.authorize({
        scope: a,
        success() {
          _this.getLocation();
        }
      });
    },
    //点击地图时
    clickMap(e) {
      console.log("点击地图时:" + e);
      console.log(e.detail.latitude);
      console.log(e.detail.longitude);
      this.latitude = e.detail.latitude;
      this.longitude = e.detail.longitude;
    },
    getLocationInfo() {
      common_vendor.index.chooseLocation({
        success: (res) => {
          console.log(res);
          if (res.errMsg == "chooseLocation:ok") {
            this.address_info = res.name + res.address;
            this.latitude = res.latitude;
            this.longitude = res.longitude;
            this.getLocationDetail();
          }
        }
      });
    },
    //获取当前所在位置的经纬度
    getLocation() {
      common_vendor.index.getLocation({
        type: "gcj02",
        success: (res) => {
          console.log(res);
          this.latitude = res.latitude.toString();
          this.longitude = res.longitude.toString();
          this.getLocationDetail();
        }
      });
    },
    //根据经纬度获取详细的地址
    getLocationDetail() {
      common_vendor.index.request({
        header: {
          "Content-Type": "application/text"
        },
        url: "https://apis.map.qq.com/ws/geocoder/v1/?location=" + this.latitude + "," + this.longitude + "&key=X4ZBZ-VNRKL-XNHPV-MN4KJ-ANQKZ-ZEBSJ",
        success: (re) => {
          console.log(re);
          if (re.statusCode == 200) {
            this.address_info_recomd = re.data.result.formatted_addresses.recommend;
            this.address_info = re.data.result.address_reference.town.title + re.data.result.address_reference.street.title + re.data.result.address_reference.landmark_l2.title;
            this.address = re.data.result.address_reference.town.title;
          } else {
            common_vendor.index.showToast({
              title: "获取地理位置失败，请重试",
              icon: "none"
            });
          }
        }
      });
    },
    //根据经纬度获取详细的地址
    // https://apis.map.qq.com/place_cloud/search/nearby?keyword=营业部西城区&location=39.940567,116.343687&radius=1000&auto_extend=1&filter=x.price>18 and x.price<=20 and x.star>=3&orderby=x.price desc&table_id=5d3581dc6ce89813ed0b2cbd&key=开发者key
    // 在地图渲染更新完成时触发
    regionchange(e) {
      if (e.type == "end" && (e.causedBy == "scale" || e.causedBy == "drag")) {
        this.mapCtx = common_vendor.index.createMapContext("mapSelected");
        this.mapCtx.getCenterLocation({
          success: (res) => {
            this.latitude = res.latitude;
            this.longitude = res.longitude;
          }
        });
      }
    },
    //地图点击事件
    markertap(e) {
      console.log("===你点击了标记点===", e);
    },
    //地图点击事件
    callouttap(e) {
      common_vendor.index.navigateTo({ url: "../../pages/game/index" });
    }
  }
};
if (!Array) {
  const _easycom_uni_nav_bar2 = common_vendor.resolveComponent("uni-nav-bar");
  const _easycom_tabbar2 = common_vendor.resolveComponent("tabbar");
  (_easycom_uni_nav_bar2 + _easycom_tabbar2)();
}
const _easycom_uni_nav_bar = () => "../../node-modules/@dcloudio/uni-ui/lib/uni-nav-bar/uni-nav-bar.js";
const _easycom_tabbar = () => "../../components/tabbar/tabbar.js";
if (!Math) {
  (_easycom_uni_nav_bar + _easycom_tabbar)();
}
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.o($options.clickLeft),
    b: common_vendor.p({
      dark: true,
      fixed: true,
      shadow: true,
      ["background-color"]: "#007AFF",
      ["status-bar"]: true,
      title: "寻梦周边",
      ["left-icon"]: "location",
      leftText: $data.address,
      leftWidth: "70px",
      rightWidth: "70px"
    }),
    c: $data.latitude,
    d: $data.longitude,
    e: $data.covers,
    f: $data.controls,
    g: common_vendor.o((...args) => $options.clickMap && $options.clickMap(...args)),
    h: common_vendor.o((...args) => $options.regionchange && $options.regionchange(...args)),
    i: common_vendor.o((...args) => $options.markertap && $options.markertap(...args)),
    j: common_vendor.o((...args) => $options.callouttap && $options.callouttap(...args))
  };
}
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "/Users/yicaohuang/Downloads/dream_star/src/pages/index/index.vue"]]);
wx.createPage(MiniProgramPage);
